import React, {useState} from 'react'
import { connect } from 'react-redux';
import { Auth } from 'library/api/auth';
import { setTokenToken } from 'library/utilities/token';
import { getAuthentication } from 'library/common/Actions/authActions';
import RestorePassModal from "./authorizationFrames/RestorePassModal"

import styles from "./index.module.scss";

import autoLogo from "resources/images/AutoLogo.svg"

function Authorization(props){

    const [modalIsOpen, setModalIsOpen] = useState(false)
    const [authProcess, setAuthProcess] = useState(false);
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [error, setError] = useState(null);
 
    const[emailError, setEmailError] = useState(false);
    const[passError, setPassError] = useState(false);

    const inputHandlers = {
        email:setEmail,
        password:setPassword,
    }
    console.log(error)
    const inputErrorHandlers = {
        error:setError
    }

    function validateForm(){
        clearErrors();
        let isValid = true;
          if (!email) {
            inputErrorHandlers['error']('Заполните поле');
            setEmailError(true);
            isValid = false;
          }
          else if (!/.+@.+\..+/i.test(email)) {
            inputErrorHandlers['error']('Email не действителен');
            setEmailError(true);
            isValid = false;
          }
          else if (!password) {
            inputErrorHandlers['error']('Заполните поле Пароль');
            setPassError(true)
            isValid = false;
          }
        return isValid;
    }

    function clearErrors(){
        inputErrorHandlers['error'](null);
        setEmailError(false);
        setPassError(false);
    }

    function handlerInput(e, field){
        inputHandlers[field](e.target.value);
        inputErrorHandlers['error'](null);
    }

    function postAuthInfo() {
        if(validateForm() && !authProcess) {
            setAuthProcess(true);
            Auth(JSON.stringify({"username":email,"password":password})).then( response => {
                if(response.data.token){
                    setTokenToken(response.data.token)
                }
                props.getAuthentication();
                setAuthProcess(false);
            }).catch(error => {
                inputErrorHandlers['error']('Неверные учетные данные')
                setAuthProcess(false);
            });
        }
    }

    return <div className={styles.authorization}>
        <div className={styles.authorization__forma}>
            <img className={styles.logotip} src={autoLogo} alt=""/>
            <input
                className={styles.input + ((emailError)? ' ' + styles.input__error:'')}
                type='email'
                placeholder="E-mail"
                value={email}
                onChange={(e) => handlerInput(e, 'email')}
                onKeyDown={e => {
                    if (e.key === 'Enter') {
                        postAuthInfo();
                    }
                  }}
            />
            <input
                className={styles.password + ((passError)? ' ' + styles.input__error:'')}
                type='password'
                placeholder="Пароль"
                value={password}
                onChange={(e) => handlerInput(e, 'password')}
                onKeyDown={e => {
                    if (e.key === 'Enter') {
                        postAuthInfo();
                    }
                  }}
            />
            <div className={styles.subWrapper}>
                <div className={styles.subWrapper__error}></div> 
                <div className={styles.subWrapper__forget} onClick={() => setModalIsOpen(true)}>
                    Забыли пароль?
                </div>
            </div>

            <button className = {styles.btn} onClick={postAuthInfo} disabled={!(email.length > 0 && password.length > 0)}>Войти</button>
        </div>
        <RestorePassModal 
        hideModal={() => setModalIsOpen(false)}
        open={modalIsOpen}/>
    </div>;
}

const mapDispatchToProps = {
    getAuthentication,
};
  
export default connect(null,mapDispatchToProps)(Authorization);
