import React from 'react'
import styles from "./index.module.scss";
import Modal from "react-modal"

function RestorePassModal(props){
    return  (<Modal
        isOpen={props.open}
        className={styles.modal}
        overlayClassName={styles.modalWrapper}
        onRequestClose={props.hideModal}
        >
        <div className={styles.header}>
            <div className={styles.title}>
                 Восстановление пароля
            </div>
        </div>
        <div className={styles.content}>
        <div className={styles.text}>
        Обратитесь к представителю компании
            по номеру <span>+7 (900) 888-88-88</span>
        </div>
        <button className={styles.btn} onClick={props.hideModal}>
            Ок
        </button>
        </div>
         </Modal>)
   
}

export default RestorePassModal