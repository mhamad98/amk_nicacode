import React from "react";
import Modal from "react-modal";
import { NavLink } from 'react-router-dom';

import styles from './index.module.scss';
import SVG from 'react-inlinesvg';

import passwordComp from '../../../../resources/images/passwordComp.svg'

export default function ProfileAutoModal(props) {
    function okClick(params) {
        props.hideModal()
        props.confirm()
        props.authorized(true)
    }
    return (
    <Modal isOpen={props.show}
    className={styles.modal}
    overlayClassName={styles.modalWrapper}
    onRequestClose={props.hideModal}
    ariaHideApp={false}>
        <div className={styles.modal__title}>Введите пароль</div>
        <div className={styles.modal__text}>Который использовали при входе</div>
        <SVG src={passwordComp} />
        <input type="password" />
        <NavLink className={styles.ok} to="/profile" onClick={()=>okClick()}>
            <div className={styles.ok__text}>Ок</div>
        </NavLink>
    </Modal>)
}