import React, {useState} from "react";
import SVG from 'react-inlinesvg';

import styles from "./index.module.scss";
import ImagePreveiw from '../../library/common/Components/CommonImagePreview'

import defaultAvatar from '../../resources/images/defaultAvatar.svg'

function Profile(props) {

    const [avatar, setAvatar] = useState(null);
    const [error, setError] = useState(null);
    console.log("./src/modules/Profile/index.jsx:13", error)
    function onPhotoChange(e){
        const file = e.target.files[0];
        if (!file) {
        return
        }
        if(file.size > 5*1024*1024){
            setError('Photo size too big')
            setTimeout(() => setError(null), 5000)
            return
        }
        setAvatar(file)
    }


    return <div className={styles.profile}>
        <div className={styles.profile__avatar}>
            <label htmlFor="file">
                {
                    avatar ? <ImagePreveiw image={avatar}/> : <SVG className={styles.avatar} src={defaultAvatar} alt=""/>
                }
                
            </label>
            <input 
            id="file"
            type="file" 
            className={styles.input} 
            onChange={(e)=> onPhotoChange(e)} 
            accept="image/*" 
            />
            <div className={styles.changePass}>Изменить пароль </div>
        </div>
        <div className={styles.profile__data}>
            <div className={styles.common}>
                <input className={styles.fio} value="Иванова Дарья Владимировна" readOnly/>
                <input className={styles.mail} value="torp.garrett@lauretta.io" readOnly/>
                <input className={styles.phone} value="+7 (928) 325-65-65" readOnly/>
            </div>
            <input type="text" className={styles.city} value="Ростов-на-Дону" readOnly/>
            <div className={styles.container}>
                <textarea className={styles.about} rows="10" cols="45" type="text" placeholder="Напишите о себе, чтобы мы узнали вас поближе …" name="" id=""/>
            </div>
        </div>
    </div>
}

export default Profile