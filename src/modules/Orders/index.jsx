import React from "react";
import SVG from 'react-inlinesvg';
import { NavLink } from 'react-router-dom';

import styles from "./index.module.scss";
import Order from "./ordersFrames/Order";
import data from "./ordersFrames/Order/data.json"

import client from "../../resources/images/client.svg";
import id from "../../resources/images/id.svg";
import orderDate from "../../resources/images/date.svg";
import quantity from "../../resources/images/quantity.svg";
import price from "../../resources/images/price.svg";
import releaseDate from "../../resources/images/date.svg";
import status from "../../resources/images/status.svg";
import add from "../../resources/images/add.svg";
import searchOrder from "../../resources/images/searchOrder.svg";
import notPaid from "../../resources/images/notPaid.svg";
import dispatched from "../../resources/images/dispatched.svg";
import done from "../../resources/images/done.svg";


function Orders(props) {
    return (
        <div className={styles.grid}>
            <div className={styles.grid__buttons}>
                <div className={styles.buttons__switch}>
                    <NavLink className={styles.switch__active} activeClassName={styles.active_active} to="/orders">Активные</NavLink>
                    <NavLink className={styles.switch__history} activeClassName={styles.history_active} to="/history">История</NavLink>
                </div>
                <div className={styles.buttons__more}>
                    <NavLink className={styles.more__newOrder} to="/orders/newOrder">
                        <SVG className={styles.newOrder__icon} src={add} />
                        <div className={styles.newOrder__text}>Новый заказ</div>
                    </NavLink>

                    <div className={styles.more__search}>
                        <input className={styles.search__text} placeholder = "Поиск" />                       
                        <SVG className={styles.search__icon} src={searchOrder} />
                    </div>

                </div>
            </div>
            <div className={styles.grid__table}>
                <div className={styles.table__titles}>
                    <div className={styles.titles__client}>
                        <SVG className={styles.client__icon} src={client} alt="client icon"/>
                        <div className={styles.client__text}>Клиент</div>
                    </div>
                    <div className={styles.titles__id}>
                        <SVG className={styles.id__icon} src={id} alt="id client"/>
                        <div className={styles.id__text}>Номер заказа</div>
                    </div>
                    <div className={styles.titles__orderDate}>
                        <SVG className={styles.orderDate__icon} src={orderDate} alt="orderDate icon"/>
                        <div className={styles.orderDate__text}>Дата заказа</div>
                    </div>
                    <div className={styles.titles__quantity}>
                        <SVG className={styles.quantity__icon} src={quantity} alt="quantity icon"/>
                        <div className={styles.quantity__text}>Кол-во упаковок</div>
                    </div>
                    <div className={styles.titles__price}>
                        <SVG className={styles.price__icon} src={price} alt="price icon"/>
                        <div className={styles.price__text}>Стоимость</div>
                    </div>
                    <div className={styles.titles__releaseDate}>
                        <SVG className={styles.releaseDate__icon} src={releaseDate} alt="releaseDate icon"/>
                        <div className={styles.releaseDate__text}>Дата отгрузки</div>
                    </div>
                    <div className={styles.titles__status}>
                        <SVG className={styles.status__icon} src={status} alt="status icon"/>
                        <div className={styles.status__text}>Статус заказа</div>
                    </div>
                </div>
                <Order orders={data.orders}/>
                <NavLink className={styles.grid__btn} to='/orders/newOrder'>
                    <button className={styles.grid__new}> + Новый заказ</button>
                </NavLink>
            </div>
        </div>
    )
}

export default Orders;