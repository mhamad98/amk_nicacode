import React from "react";
import SVG from 'react-inlinesvg';
import { NavLink } from 'react-router-dom';

import styles from "./index.module.scss";
import done from '../../../../resources/images/done.svg'


function Order(props) {
    return <div className={styles.table__records}>
        {
            props.orders.map(order =>{
                return <div className={styles.table__record}>
                    <div className={styles.record__client}>{order.client}</div>
                    <div className={styles.record__id}>{order.id}</div>
                    <div className={styles.record__orderDate}>{order.orderDate}</div>
                    <div className={styles.record__quantity}>{order.quantity}</div>
                    <div className={styles.record__price}>{order.price}руб</div>
                    <div className={styles.record__releaseDate}>{order.releaseDate}</div>
                    <div className={styles.record__status}>
                        <SVG className={styles.status__icon} src={done} />
                        <div className={styles.status__text}>{order.status}</div>
                    </div>
                </div>
            })
        }
    </div>
}

export default Order;