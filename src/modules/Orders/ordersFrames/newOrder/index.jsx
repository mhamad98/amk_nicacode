import React from "react";
import SVG from 'react-inlinesvg';
import { NavLink } from 'react-router-dom';

import styles from "./index.module.scss";
import Walls from "library/common/Components/Walls";
import ClientInfo from 'library/common/Components/ClientInfo';
import LocationInfo from 'library/common/Components/LocationInfo'
import DeliveryInfo from 'library/common/Components/DeliveryInfo'
import OrderInfo from 'library/common/Components/OrderInfo'
import GlueInfo from 'library/common/Components/GlueInfo'


function newOrder(props) {
    return <div className={styles.grid}>
        <Walls />
        <ClientInfo />
        <LocationInfo />
        <DeliveryInfo/>
        <OrderInfo />
        <GlueInfo />
    </div>
}

export default newOrder;