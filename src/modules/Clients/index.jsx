import React from "react";
import SVG from 'react-inlinesvg';

import styles from "./index.module.scss";
import Client from "./clientsFrames/Client"
import data from '../Clients/clientsFrames/Client/data.json';

import FIO from '../../resources/images/FIO.svg';
import search from '../../resources/images/search.svg';
import phone from '../../resources/images/phone.svg';
import mail from '../../resources/images/mail.svg';
import date from '../../resources/images/date.svg';

function Clients(props) {
    return (
        <div className={styles.grid}>
            <div className={styles.grid__search}> 
                <input className={styles.search__text} placeholder = "Поиск" />                       
                <SVG className={styles.search__icon} src={search} />
            </div>
            <div className={styles.grid__table}>
                <div className={styles.table__titles}>
                    <div className={styles.titles__FIO}>
                        <SVG className={styles.FIO__icon} src={FIO} alt="FIO icon"/>
                        <div className={styles.FIO__text}>ФИО</div>
                    </div>
                    <div className={styles.titles__phone}>
                        <SVG className={styles.phone__icon} src={phone} alt="phone client"/>
                        <div className={styles.phone__text}>Номер телефона</div>
                    </div>
                    <div className={styles.titles__mail}>
                        <SVG className={styles.mail__icon} src={mail} alt="mail icon"/>
                        <div className={styles.mail__text}>E-mail</div>
                    </div>
                    <div className={styles.titles__date}>
                        <SVG className={styles.date__icon} src={date} alt="date icon"/>
                        <div className={styles.date__text}>Дата последнего заказа</div>
                    </div>
                </div>
                <Client 
                clients = {data.clients}
                />
            </div>
        </div>
    )
}

export default Clients;