import React from "react";

import styles from "./index.module.scss";

function client(props) {
    return <div className={styles.table__records}>
    {
        props.clients.map(client => {
            return (
                <div className={styles.table__record}>
                    <div className={styles.record__FIO}>{client.FIO}</div>
                    <div className={styles.record__phone}>{client.phone}</div>
                    <div className={styles.record__mail}>{client.mail}</div>
                    <div className={styles.record__date}>{client.date}</div>
                </div>
            )
        })
    }
    </div>
}

export default client;