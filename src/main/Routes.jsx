import React from 'react';
import { Route, Switch, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { getAuthentication } from 'library/common/Actions/authActions';

import Header from '../library/common/Components/Header';
import Menu from '../library/common/Components/Menu';
import Orders from '../modules/Orders';
import newOrder from '../modules/Orders/ordersFrames/newOrder'
import Clients from '../modules/Clients';
import Authorization from '../modules/Authorization';
import Profile from '../modules/Profile'

class AppRouter extends React.Component {
  componentDidMount() {
    this.props.getAuthentication()
  }
  render() {
    let routes = (
      <Switch>
        <Route exact path='/' component={Authorization} />
      </Switch>
    )
    if (this.props.isLoggedIn) {
      routes = (
        <>
        <Route path="*" component={Header} />
        <Switch>
          <Route exact path='/' component={Orders} />
          <Route exact path='/clients' component={Clients} />
          <Route exact path='/profile' component={Profile} />
          <Route exact path='/orders' component={Orders} />
          <Route exact path='/orders/newOrder' component={newOrder} />
        </Switch>
        <Route path="*" component={Menu} />
        </>
      )
    }

    return <div>{routes}</div>
  }
}

AppRouter.defaultProps = {
  userInfo: {},
};

const mapStateToProps = state => ({
  isLoggedIn: state.authReducer.isLoggedIn,
});

const mapDispatchToProps = {
  getAuthentication,
};


export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
  (AppRouter),
);