let API_BASE_URI;
let url = window.location.href;

if(url.indexOf('http://localhost:3000') !== -1){
    API_BASE_URI = "http://localhost:3000";
 } else if (url.indexOf('https://amk.nicecode.biz/') !== -1){
  API_BASE_URI = "https://amk.nicecode.biz/";
 }

 else if (url.indexOf('192.168.0.103:3000') !== -1){
  API_BASE_URI = "https://amk-api.nicecode.biz";
 }

 export default {
  API_BASE_URI
};