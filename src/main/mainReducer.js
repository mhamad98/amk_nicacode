import { combineReducers } from 'redux';

import AuthReducer from '../library/common/Reducers/authReducer';

export default combine();

export function combine() {
    return combineReducers({
      authReducer: AuthReducer,
    });
  }
  