import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';

import store from './main/store/configureStore';
import { getAuthentication } from './library/common/Actions/authActions';
import Routes from './main/Routes'

import './resources/styles/global.scss';

store.dispatch(getAuthentication());

function App() {
  return (
    <Provider store={store}>
       <BrowserRouter>
          <Routes />
       </BrowserRouter>
      </Provider>
  );
}

export default App;
