
const proxy = require('http-proxy-middleware');

module.exports = function(app) {
  app.use(proxy('/api/', {
    target: "https://amk-api.nicecode.biz",
    changeOrigin: true,
    secure: true
  }))
};

