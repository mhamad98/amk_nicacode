import * as actionTypes from '../Constants/commonConstants';

const initialState = {
  isLoggedIn: false,
  user: {},
  logOutModal: false
};

const AuthReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.SET_AUTH_TOKEN:
      return {
        ...state,
        user: action.payload,
        isLoggedIn: true,
      };
    case actionTypes.GET_AUTH_TOKEN:
      return {
        ...state,
        user: action.payload.token,
        isLoggedIn: action.payload.isLoggedIn,
      };
    case actionTypes.REMOVE_AUTH_TOKEN:
      return {
        ...state,
        user: action.payload,
        isLoggedIn: false,
      };
      case actionTypes.SHOW_OUT_MODAL:
      return {
        ...state,
        logOutModal: action.payload,
      };
    default:
      return state;
  }
};

export default AuthReducer;
