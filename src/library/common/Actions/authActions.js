import * as aT from 'library/common/Constants/commonConstants';
import { setToken, getTokenToken, removeToken, removeTokenToken } from 'library/utilities/token';

export const setAuthentication = data => dispatch => {
  setToken(data);

  dispatch({
    type: aT.SET_AUTH_TOKEN,
    payload: data,
  });
};

export const getAuthentication = () => dispatch => {
  const user = getTokenToken();
  return new Promise((resolve) => {
    if (user) {
      const tokenObject = {
        token: user,
        isLoggedIn: true,
      };

      dispatch({
        type: aT.GET_AUTH_TOKEN,
        payload: tokenObject,
      });

      resolve(user);
    } 
  });
};

export const removeAuthentication = () => dispatch => {
  removeToken();
  removeTokenToken();
  dispatch({
    type: aT.REMOVE_AUTH_TOKEN,
    payload: {},
  });
};


export const showOutModal = flag => dispatch => {
  dispatch({
    type: aT.SHOW_OUT_MODAL,
    payload: flag,
  });
};


