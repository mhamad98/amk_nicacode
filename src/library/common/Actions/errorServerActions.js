import * as aT from 'library/common/Constants/commonConstants';

export const showErrorServer = (data) => dispatch => {
    dispatch({
      type: aT.SHOW_ERROR_SERVER,
      payload: data,
    });
};
  