export const URLS = {
    logout: '/user/logout',
    refreshToken: '/oauth/token',
  };

  export const SHOW_ERROR_SERVER = 'SHOW_ERROR_SERVER';
  export const SET_AUTH_TOKEN = 'SET_AUTH_TOKEN';
  export const GET_AUTH_TOKEN = 'GET_AUTH_TOKEN';
  export const REMOVE_AUTH_TOKEN = 'REMOVE_AUTH_TOKEN';
  export const SHOW_OUT_MODAL = 'SHOW_OUT_MODAL';