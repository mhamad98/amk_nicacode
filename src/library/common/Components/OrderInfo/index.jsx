import React, { useState } from "react";
import SVG from 'react-inlinesvg';

import styles from "./index.module.scss";

import weight from 'resources/images/weightkg.svg'
import wallSquare from 'resources/images/wallSquare.svg'
import price from 'resources/images/price.svg'

function OrderInfo(props) {
    return <div className={styles.OrderInfo}>
        <div className={styles.data}>
            <div className={styles.wall}>
                <div className={styles.packages}>
                    <SVG src={weight} />
                    <div className={styles.text}>Упаковок: </div>
                    <div className={styles.subtext}> 0 шт</div>
                </div>
                    <div className={styles.slash} />
                <div className={styles.square}>
                    <SVG src={wallSquare}/>
                    <div className={styles.text}>Площадь: </div>
                    <div className={styles.subtext}> 0 м2</div>
                </div>
            </div>
            <div className={styles.slash} />
            <div className={styles.price}>
                <SVG src={price}/>
                <div className={styles.text}>Итого к оплате: </div>
                <div className={styles.subtext}> 0 руб</div>
            </div>
        </div>
        <button className={styles.arrange}>Оформить заказ</button>
    </div>
}

export default OrderInfo