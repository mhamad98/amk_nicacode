import React, { useState } from "react";
import Modal from "react-modal";

import styles from './index.module.scss';
import SVG from 'react-inlinesvg';

import color101 from 'resources/images/color101.png';
import color102 from 'resources/images/color102.png';
import color103 from 'resources/images/color103.png';
import color201 from 'resources/images/color201.png';
import color202 from 'resources/images/color202.png';
import color203 from 'resources/images/color203.png';
import color204 from 'resources/images/color204.png';
import color301 from 'resources/images/color301.png';
import color302 from 'resources/images/color302.png';
import color303 from 'resources/images/color303.png';
import color401 from 'resources/images/color401.png';
import color402 from 'resources/images/color402.png';
import color403 from 'resources/images/color403.png';
import color404 from 'resources/images/color404.png';
import check from 'resources/images/check.svg';
import arrowHeight from 'resources/images/arrowHeight.svg';
import arrowWidth from 'resources/images/arrowWidth.svg';
import moreInfo from 'resources/images/moreInfo.svg';
import blockBox from 'resources/images/blockBox.svg';
import brickBox from 'resources/images/brickBox.jpg';
import domtoimage from 'dom-to-image';

export default function ModalWall(props) {  

    

    const blockData = {
            image:blockBox,
            blockHeight:1250,
            blockWidth:1050,
            blockElem:"400x190",
            listSquare:"0,94",
            blockWeight:"1,8",
            allWeight:"18",
            size:"1100x250x130",
            lists:10,
            square:"9,4"
        }

    const brickData = {
        image:brickBox,
        blockHeight:1250,
        blockWidth:995,
        blockElem:"250x65",
        listSquare:"0,9",
        blockWeight:"1,7",
        allWeight:"17",
        size:"1000x340x80",
        lists:10,
        square:"9"
    }
    
    const AllColors = [
        color101, color102, color103, color201, color202,
        color203, color204, color301, color302, color303,
        color401, color402, color403, color404
    ]
    const colorMixBlock = [
        [
            color101, color102, color103
        ],
        [
            color201, color202, color203
        ],
        [
            color302, color302, color303
        ],
        [
            color401, color402, color403
        ]
    ]

    const colorMixBrick = [
        [
            color101, color102, color103, color101
        ],
        [
            color201, color202, color203, color204
        ],
        [
            color302, color302, color303, color301
        ],
        [
            color401, color402, color403, color404
        ]
    ]

    const MainColors = [
        color102, color201, color301, color401
    ]
    const colorId = ["101", "102", "103", "201", "202", "203", "204", "301", "302", "303", "401", "402", "403", "404" ]
    

    const [block, setBlock] = useState([
        {border: "0.8px dashed #CBCBCB"},
        {border: "0.8px dashed #CBCBCB"},
        {border: "0.8px dashed #CBCBCB"},
        {border: "0.8px dashed #CBCBCB"},
        {border: "0.8px dashed #CBCBCB"},
        {border: "0.8px dashed #CBCBCB"},
        {border: "0.8px dashed #CBCBCB"},
        {border: "0.8px dashed #CBCBCB"},
        {border: "0.8px dashed #CBCBCB"},
        {border: "0.8px dashed #CBCBCB"},
        {border: "0.8px dashed #CBCBCB"},
        {border: "0.8px dashed #CBCBCB"}
    ])

    let image;

    const [colors, setColor] = useState([
        {src:color101},
        {src:color102},
        {src:color103},
        {src:color201},
        {src:color202},
        {src:color203},
        {src:color204},
        {src:color301},
        {src:color302},
        {src:color303},
        {src:color401},
        {src:color402},
        {src:color403},
        {src:color404}
    ])
    const [ownDesign, setOwn] = useState([]);
    const [currentColArr, setCurrColArr] = useState([])
    const [currentC, setCurrentC] = useState(0);
    const [type, setType] = useState("block");
    const [displayStatus, setStatus] = useState("none");
    const [design, setDesign] = useState("single");
    const node = document.getElementById('my-node');
    
    function saveImg() {
        domtoimage.toSvg(node)
        .then(function (dataUrl) {
            const img = new Image();
            
            img.src = dataUrl;

            
            image = img.src
        })
        .catch(function (error) {
            console.error('oops, something went wrong!', error);
        });
    }
    

    function drawBlocks(color,type) {
        let newBlocks = [];
        if (design == "single") {
            if (type == "block") {
                for (let index = 0; index < 12; index++) {
                    newBlocks.push({
                        src: colors[color].src,
                    })
                }
            }
            else if (type == "brick" || "klincker") {
                for (let index = 0; index < 48; index++) {
                    newBlocks.push({
                        src: colors[color].src,
                    })
                }
            }
            props.updateColor(AllColors[color]);
        }
        else if (design == "mix") {
            if (type == "block") {
                let j = 0;
                for (let index = 0; index < 15; index++) {
                    if (j < 3) {
                        newBlocks.push({
                            src: colorMixBlock[color][j],
                        })
                        j++;
                    }
                    else {
                        j = 0;
                    }
                }
            }
            else if (type == "brick" || "klincker") {
                let j = 0;
                for (let index = 0; index < 60; index++) {
                    if (j < 4) {
                        newBlocks.push({
                            src: colorMixBrick[color][j],
                        })
                        j++;
                    }
                    else {
                        j = 0;
                    }
                }
            }
            props.updateColor(colorMixBrick[color][0]);
        }
        else if (design == "own") { 

            let newCurrColArr = currentColArr.slice()
            let newOwnDesign = ownDesign.slice();
            {
                if (newCurrColArr.includes(color)) {
                newCurrColArr.pop()
                }

                else {
                    if (currentColArr.length < 3) {
                    newCurrColArr.push(color)
                    
                    }
                }
            }

            if (newOwnDesign.includes(AllColors[color])) {
                newOwnDesign.pop()
            }
            else {
                if (newOwnDesign.length < 3) {
                    newOwnDesign.push(AllColors[color]);
                }
            }
            let j = 0;
            let k = 0;

            if (type == "block") {
                for (let index = 0; index < 12; index++) {
                    if (j >= newOwnDesign.length) {
                         j=0;
                    }  
                    if (newOwnDesign.length == 2) {
                        newBlocks.push({
                            src: newOwnDesign[k],
                        })
                        if (index == 1) {k++}
                        else if (index == 4) {k--}
                        else if (index == 6) {k++}
                        else if (index == 8) {k--}
                        else if (index == 9) {k++}
                        else if (index == 10) {k--}
                    }
                    else {
                        newBlocks.push({
                            src: newOwnDesign[j],
                        })
                    }
                    j++;
                }
            }
            else if (type == "brick" || type == "klincker") {
                for (let index = 0; index < 48; index++) {
                    if (j >= newOwnDesign.length) {
                        j=0;
                    }  
                    
                    if (newOwnDesign.length == 2) {
                        newBlocks.push({
                            src: newOwnDesign[k],
                        })
                        if (index == 1) {k++} else if (index == 2) {k--}else if (index == 3) {k++}else if (index == 4) {k--}else if (index == 5) {k++}
                        else if (index == 7) {k--}else if (index == 7) {k--}else if (index == 8) {k++}else if (index == 9) {k--}else if (index == 10) {k++}else if (index == 12) {k--}
                        else if (index == 13) {k++}else if (index == 14) {k--}else if (index == 16) {k++}else if (index == 17) {k--}else if (index == 18) {k++}
                        else if (index == 19) {k--}else if (index == 21) {k++}else if (index == 24) {k--}else if (index == 25) {k++}else if (index == 28) {k--}
                        else if (index == 30) {k++}else if (index == 31) {k--}else if (index == 34) {k++}else if (index == 37) {k--}else if (index == 39) {k++}
                        else if (index == 40) {k--}else if (index == 42) {k++}else if (index == 43) {k--}else if (index == 45) {k++}
                    }
                    else if (newOwnDesign.length == 3) {
                        newBlocks.push({
                            src: newOwnDesign[k],
                        })
                        if (index == 1) {k=0} else if (index == 2) {k=1}else if (index == 3) {k=2}else if (index == 4) {k=0}else if (index == 5) {k=0}
                        else if (index == 6) {k=2}else if (index == 7) {k=0}else if (index == 8) {k=1}else if (index == 9) {k=2}else if (index == 10) {k=0}else if (index == 12) {k=1}
                        else if (index == 13) {k=2}else if (index == 14) {k=0}else if (index == 16) {k=1}else if (index == 17) {k=2}else if (index == 18) {k=0}
                        else if (index == 19) {k=1}else if (index == 21) {k=2}else if (index == 24) {k=0}else if (index == 25) {k=1}else if (index == 28) {k=2}
                        else if (index == 30) {k=0}else if (index == 31) {k=1}else if (index == 34) {k=2}else if (index == 37) {k=0}else if (index == 39) {k=1}
                        else if (index == 40) {k=2}else if (index == 42) {k=0}else if (index == 43) {k=1}else if (index == 45) {k=2}
                    }
                    else {
                        newBlocks.push({
                            src: newOwnDesign[j],
                        })
                    }
                   j++;
                }
            }
            setCurrColArr(newCurrColArr)
            setOwn(newOwnDesign);
            props.updateColor(AllColors[color]);
        }
        
        setBlock(newBlocks);
        setType(type);
        setCurrentC(color);
        
        
    }

    function drawDesign(design) {
        let newDesigns = [];
        if (design == "single") {
            for (let index = 0; index < 14; index++) {
                newDesigns.push({
                    src: AllColors[index]
                })
            }
        }
        else if (design == "mix") {
            for (let index = 0; index < 4; index++) {
                newDesigns.push({
                    src: MainColors[index],
                })
            }
        }
        else if (design == "own") {
            for (let index = 0; index < 14; index++) {
                newDesigns.push({
                    src: AllColors[index],
                })
            }
        }
        setColor(newDesigns)
        setDesign(design)
        
    }

    function showInfo() {
        if (displayStatus == "none") {
            setStatus("block")
        }
        else if (displayStatus == "block") {
            setStatus("none")
        }
    }

    return <Modal isOpen={props.show}
                className={styles.modal}
                overlayClassName={styles.modalWrapper}
                onRequestClose={props.hideModal}>
        <div className={styles.header}>
            <button onClick={() => 
                    {
                        
                        saveImg();
                        // delay for saveImg() to save the img 
                        setTimeout(function() {props.updateData(false,image,type,ownDesign,design)}, 750);
                        
                    }
                } className={styles.header__finish}>Готово</button>
        </div>
        <div className={styles.content}>
            <div className={styles.content__type}>
                    <div className={styles.radio}>
                        <input type="radio" id="block" name="selector" onClick={()=>drawBlocks(currentC,"block")}/><label htmlFor="block"><div className={styles.btn__text}>Блок</div></label>
                        <input type="radio" id="kirpich" name="selector" onClick={()=>drawBlocks(currentC,"brick")}/><label htmlFor="kirpich"><div className={styles.btn__text}>Кирпич</div></label>
                        <input type="radio" id="klincker" name="selector" onClick={()=>drawBlocks(currentC,"klincker")}/><label htmlFor="klincker"><div className={styles.btn__text}>Клинкер</div></label>
                    </div>
                <div className={styles.type__drawing}>
                    <div className={styles.upperDraw}>
                        <div className={styles.drawing__height}>
                            <SVG src={arrowHeight}></SVG>
                            <div className={styles.arrow__text}>{type === "block" ? blockData.blockHeight : brickData.blockHeight} мм</div>
                        </div>
                        <div className={styles.drawing__blocks} id="my-node">
                            {
                                block.map((block, index) =>
                                    <div className={styles.block + " " + ((type ==  "brick" || type ==  "klincker") ? styles.brick : "")} src={block.src} key={index} alt='' 
                                    style={{border:block.border, backgroundImage: `url(${block.src})`}}/>
                                )
                            }
                        </div>
                    </div>
                    <div className={styles.drawing__width}>
                        <SVG src={arrowWidth}></SVG>
                        <div className={styles.arrow__text}>{type === "block" ? blockData.blockWidth : brickData.blockWidth}мм</div>
                    </div>
                </div>
            </div>
            <div className={styles.content__design}>  
                <div className={styles.radio}>
                    <input type="radio" id="des1" name="selector1" onClick={()=>drawDesign("single")}/><label htmlFor="des1"><div className={styles.btn__text}>Однотонные</div></label>
                    <div className={styles.border__left} />
                    <input type="radio" id="des2" name="selector1" onClick={()=>drawDesign("mix")}/><label htmlFor="des2"><div className={styles.btn__text}>Микс</div></label>
                    <div className={styles.border__right} />
                    <input type="radio" id="des3" name="selector1" onClick={()=>drawDesign("own")}/><label htmlFor="des3"><div className={styles.btn__text}>Свой дизайн</div></label>
                </div>
                <div className={styles.container__colors}>
                    <div className={styles.colors}>
                            {
                                colors.map((color, index) =>
                                    <div className={styles.color} key={index}>
                                        <div 
                                        style={{ backgroundImage: `url(${color.src})`}}
                                        className={styles.block__C + " " + ((((index === currentC) && ((design == "single") || (design == "mix"))) || ((design == "own") && (currentColArr.includes(index)) )) ? styles.block__C_active : "")}
                                        onClick={()=> drawBlocks(index,type)}
                                        >
                                            <div className={styles.block__id}>
                                                {colorId[index]}
                                            </div>
                                        <div className={styles.check} >
                                            <SVG src={check} />
                                        </div>
                                        </div>
                                    </div>
                                )
                            }
                            
                    </div>
                </div>
                <div className={styles.more} onClick={()=>showInfo()}>
                    <div className={styles.more__text}>Информация</div>
                    <SVG className={styles.more__icon + " " + (displayStatus == "block" ? styles.more__icon_active : "")} src={moreInfo}/>
                </div>
            </div>
        </div>
        <div className={styles.information} style={{display: displayStatus}}>
            <div className={styles.information__content}>
                <img className={styles.box} src={type === "block" ? blockData.image : brickData.image}/>
                <div className={styles.details}>
                    <div className={styles.detail}>
                        <div className={styles.detail__description}>Размер листа:</div>
                        <div className={styles.detail__data}>{type === "block" ? blockData.blockHeight : brickData.blockHeight}x{type === "block" ? blockData.blockWidth : brickData.blockWidth} мм</div>
                    </div>
                    <div className={styles.detail}>
                        <div className={styles.detail__description}>Размер плитки под {}:</div>
                        <div className={styles.detail__data}>{type === "block" ? blockData.blockElem : brickData.blockElem} мм</div>
                    </div>
                    <div className={styles.detail}>
                        <div className={styles.detail__description}>Полезная площадь 1 листа:</div>
                        <div className={styles.detail__data}>{type === "block" ? blockData.listSquare : brickData.listSquare} м2</div>
                    </div>
                    <div className={styles.detail}>
                        <div className={styles.detail__description}>Вес 1 листа:</div>
                        <div className={styles.detail__data}>{type === "block" ? blockData.blockWeight : brickData.blockWeight} кг</div>
                    </div>
                    <div className={styles.detail}>
                        <div className={styles.detail__description}>Вес упаковки:</div>
                        <div className={styles.detail__data}>{type === "block" ? blockData.allWeight : brickData.allWeight} кг</div>
                    </div>
                    <div className={styles.detail}>
                        <div className={styles.detail__description}>Размер упаковки:</div>
                        <div className={styles.detail__data}>{type === "block" ? blockData.size : brickData.size} мм</div>
                    </div>
                    <div className={styles.detail}>
                        <div className={styles.detail__description}>В упаковке:</div>
                        <div className={styles.detail__data}>{type === "block" ? blockData.lists : brickData.lists} листов - {type === "block" ? blockData.square : brickData.square} м2</div>
                    </div>
                </div>
            </div>
            <div className={styles.information__more}>
                Возможно приобретение отдельных элементов АМК поштучно без фасадной сетки (для ремонта, декора и т.д.)
            </div>
        </div>
    </Modal>
    
}