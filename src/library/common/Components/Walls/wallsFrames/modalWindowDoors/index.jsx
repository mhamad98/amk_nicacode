import React, { useState } from "react";
import Modal from "react-modal";

import styles from './index.module.scss';
import SVG from 'react-inlinesvg';
import delete1 from 'resources/images/delete1.svg';
import closeDoor from 'resources/images/closeDoor.svg'

export default function ModalWindowDoors(props) {

    const [doors , setDoors] = useState([{
        width:0,
        height:0,
        quantity:0
    }])

    function inputHaddler(e, index, type) {
        let newD = doors.slice()
        newD[index][type] = e.target.value;
        setDoors(newD)
    }

    function addDoor() {
        let newD = doors.slice()
        newD.push({
            width:0,
            height:0,
            quantity:0
        })
        setDoors(newD)
    }

    function finish() {
        let AllSquare = 0;
        for (let index = 0; index < doors.length; index++) {
            AllSquare = parseInt(AllSquare) + (parseInt(doors[index].quantity)*parseInt(doors[index].width)*parseInt(doors[index].height))
        }
        let AllQuantity = 0;
        for (let index = 0; index < doors.length; index++) {
            AllQuantity = parseInt(AllQuantity) + parseInt(doors[index].quantity)
        }
        props.updateDoorsData(false,AllQuantity,AllSquare)
    }

    function deleteDoor(index) {
        let newD = doors.slice()
        if (doors.length === 1) {
            doors[index].width = 0;
            doors[index].height = 0;
            doors[index].quantity = 0;
        }
        else {
            newD.splice(index,1)
        }
        setDoors(newD)
    }

    return <Modal isOpen={props.show}
                className={styles.modal}
                overlayClassName={styles.modalWrapper}
                onRequestClose={props.hideModal}>
        <div className={styles.header}>
            <div className={styles.header__title}>Окна / Двери</div>
            <button className={styles.header__finish} onClick={()=>finish()}>Ок</button>
            <SVG className={styles.header__finish1} src={closeDoor} onClick={()=>finish()} />
        </div>
        <div className={styles.titles}>
            <div className={styles.title}>Ширина</div>
            <div className={styles.title}>Высота</div>
            <div className={styles.title}>Кол-во</div>
        </div>
        <div className={styles.content}>
            <div className={styles.content__container}>
                {     
                    doors.map((door,index)=>       
                        <div className={styles.content__data}>
                            <div className={styles.width}>
                                <input className={styles.input} value={door.width} in onChange={e => inputHaddler(e, index, 'width')}/>
                                <div className={styles.symbol}>м</div>
                            </div>
                            <div className={styles.height}>
                                <input className={styles.input} value={door.height} onChange={e => inputHaddler(e, index, 'height')}/>
                                <div className={styles.symbol}>м</div>
                            </div>
                            <div className={styles.quantity}>
                                <input className={styles.input} value={door.quantity} onChange={e => inputHaddler(e, index, 'quantity')}/>
                                <div className={styles.symbol}>шт</div>
                            </div>
                            <SVG className={styles.delete} src={delete1} onClick={() => deleteDoor(index)}/>
                        </div>
                )}
            </div>
            <div className={styles.footer}>
                <div className={styles.content__text}>* Вычитаются из общей площади</div>
                <button className={styles.content__add} onClick={()=>addDoor()}>+ Добавить</button>
            </div>
            <div className={styles.ready}>
                <div className={styles.ready__btn} onClick={()=>finish()}>
                    <div className={styles.text}>Готово</div>
                </div>
            </div>
        </div>
    </Modal>
}