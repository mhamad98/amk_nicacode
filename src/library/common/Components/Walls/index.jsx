import React, { useState } from "react";
import SVG from 'react-inlinesvg';

import styles from "./index.module.scss";
import ModalWall from "./wallsFrames/modalWall";
import ModalWindowDoors from "./wallsFrames/modalWindowDoors";

import wall from "../../../../resources/images/wall.svg";
import wallWidth from "../../../../resources/images/wallWidth.svg";
import wallHeight from "../../../../resources/images/wallHeight.svg";
import doorsWindows from "../../../../resources/images/doorsWindows.svg";
import wallSquare from "../../../../resources/images/wallSquare.svg";
import wallBox from "../../../../resources/images/wallBox.svg";
import plus from "../../../../resources/images/plus.svg";
import close from "../../../../resources/images/close.svg";


function Walls(props) {

    const [w, setW] = useState([
        {
            src: wall,
            title: "",
            Zindex: 1,
            type: '',
            color: ["#F5F7FB"],
            width: '',
            height: '',
            quantity: '',
            doorSquare: 0,
            boxShadow:"0px 8px 12px rgba(16, 23, 30, 0.1)"
        }
    ])
   
    const [ currentW, setCurrentW] = useState(0);
    const [ modalWall, setModalWall ] = useState(false);
    const [ ModalWD, setModalWindowDoors ] = useState(false);
    const [ wallColor, setWallolor ] = useState();
    const [ addBar, setAddBar ] = useState('none');

    function openModal() {
        setModalWall(true);
    }


    function closeModal() {
        setModalWall(false);
    }

    function openModalWindowDoors() {
        setModalWindowDoors(true);
    }


    function closeModalWindowDoors() {
        setModalWindowDoors(false);
    }

    function inputHaddler(e,index, type) {
        let newW = w.slice()
        newW[index][type] = e.target.value;
        setW(newW)
    }
    function addWall() {
        let newW = w.slice();
        newW[currentW].boxShadow="none"
        newW.push({
            src: wall,
            title: "",
            ZIndex: 10,
	        type: '',
            color: ["#F5F7FB"],
            width: '',
            height: '',
            quantity: '',
            doorSquare:0,
            boxShadow:"0px 8px 12px rgba(16, 23, 30, 0.1)"
        });
        newW[currentW].ZIndex = 1;
	    setW(newW);
        setCurrentW(newW.length - 1);
        let allWidth = document.querySelector('#navWid').offsetWidth;
        let itemWidth = document.querySelector('#itemWidth').offsetWidth;
        let AllItems = 0;
        for(let index = 0; index <= w.length; index++) {
            AllItems = AllItems + itemWidth;
        }
        if (AllItems>=allWidth) {
            setAddBar('flex')
        }
        else {
            setAddBar('none')
        }
    }

    function changeZIndex(index){
        let newW = w.slice();
        if (newW[index].ZIndex !== undefined) {
            newW[index].ZIndex = 10;
        }
        if (newW[currentW] !== undefined) {
            newW[currentW].ZIndex = 1;
        }
		setW(newW);
		setCurrentW(index);
    }
    
    function removeWall(index) {
        let allWidth = document.querySelector('#navWid').offsetWidth;
        let itemWidth = document.querySelector('#itemWidth').offsetWidth;
        let AllItems = 0;
        for(let index = 0; index <= w.length; index++) {
            AllItems = AllItems + itemWidth;
        }
        if (AllItems<=allWidth) {
            setAddBar('none')
        }
        let newW = w.slice()
        if (newW.length === 1) {
            newW[index].src = wall;
            newW[index].title = "";
            newW[index].Zindex = 1;
            newW[index].type = '';
            newW[index].color.splice(0,3);
            newW[index].color[0] = "#F5F7FB";
            newW[index].width = '';
            newW[index].height = '';
            newW[index].quantity = '';
            newW[index].doorSquare = 0;
        }
        else {
            newW.splice(index,1)
        }
        setW(newW)

    }

    let updateData = (value,img,type,ownDesign,design) => {
        setModalWall(value);
        let newW = w.slice();
        newW[currentW].type = type;
        newW[currentW].color.splice(0,ownDesign.length + 1);
        if (design === "own") {
            for (let index = 0; index < ownDesign.length; index++) {
                newW[currentW].color[index] = ownDesign[index]
            }   
        }
        else {
            newW[currentW].color[0] = wallColor;
        }
        newW[currentW].src = img;
        setW(newW)

    }


    let updateColor = (color) => {
        setWallolor(color);
    }

    let updateDoorsData = (value,AllQuantity,AllSquare) => {
        let newW = w.slice();
        newW[currentW].quantity = AllQuantity;
        newW[currentW].doorSquare = AllSquare;
        setW(newW)
        setModalWindowDoors(value)
    }


    return <div className={styles.walls}>
        <div className={styles.navigation} id="navWid">
        <ModalWall
            updateData={updateData}
            show={modalWall}
            hideModal={closeModal}
            updateColor={updateColor}
            >
            </ModalWall>
        <ModalWindowDoors
            show={ModalWD}
            hideModal={closeModalWindowDoors}
            updateDoorsData={updateDoorsData}
            >
            </ModalWindowDoors>
            {
                w.map((item,index) =>
                    <div className={styles.navigation__group} key={index} id="itemWidth">   
                            <div className={styles.navigation__item + " " + (index === currentW ? styles.navigation__item_active : "")}
                            
                            >
                                <div className={styles.text} onClick={()=> changeZIndex(index)}>
                                    <div className={styles.text__container}>
                                        Стена {index + 1}
                                    </div> 
                                </div>
                                <SVG className={styles.navigation__close + " " + (index === currentW ? styles.navigation__close_active : "")} 
							    onClick={() => removeWall(index)}
                                src={close}>
				   		        </SVG>
                            </div>
                            
                    </div>
            )}
            <div className={styles.container__plus} onClick={() => addWall()} src={plus}>
                <SVG className={styles.navigation__plus} onClick={() => addWall()} src={plus} />
            </div>
        </div>
        <div className={styles.container__scroll} style={{display:addBar}}>
                <SVG className={styles.navigation__plus} onClick={() => addWall()} src={plus} />
        </div>
        {
            w.map((w,index) => 
                <div className={styles.walls__info} key={index} style={{zIndex: w.ZIndex, boxShadow:w.boxShadow}}>
                    <div className={styles.info__choose} onClick={()=>{openModal()}}>
                        <img className={styles.wall} src={w.src === undefined ? wall : w.src }></img>
                        <div className={styles.type}>
                            <div className={styles.text}>Вид элемента</div>
                                <div className={styles.input}>
                                    <div>
                                        {(w.type === "block") ? "Блок" : w.type === "brick" ? "Кирпич" : w.type === "klincker" ? "Клинкер" : ""}
                                    </div>
                                </div>
                        </div>
                        <div className={styles.color}>
                            <div className={styles.text}>Цвет элемента</div>
                                <div className={styles.allColors}>
                                    {
                                        w.color.map((c,index)=>
                                            <div className={styles.input} style={{width:w.color.length === 1 ? 100 : w.color.length === 2 ? 75 : 54, backgroundImage: `url(${c})`}}></div>
                                        )
                                    
                                    }
                                </div>
                        </div>
                    </div>
                    <div className={styles.properties}>
                        <div className={styles.info__width}>
                            <div className={styles.title}>
                                <SVG className={styles.title__icon} src={wallWidth}></SVG>
                                <div className={styles.title__text}>Ширина</div>
                            </div>
                            <input className={styles.input} value={w.width} onChange={e => inputHaddler(e, index, 'width')}/>
                            <div className={styles.value}>м</div>
                        </div>
                        <div className={styles.info__height}>
                            <div className={styles.title}>
                                <SVG className={styles.title__icon} src={wallHeight}></SVG>
                                <div className={styles.title__text}>Высота</div>
                            </div>
                            <input className={styles.input} value={w.height} onChange={e => inputHaddler(e, index, 'height')}/>
                            <div className={styles.value}>м</div>
                        </div>
                        <div className={styles.info__quantity}>
                            <div className={styles.title}>
                                <SVG className={styles.title__icon} src={doorsWindows}></SVG>
                                <div className={styles.title__text}>Окна / Двери</div>
                            </div>
                            
                            <div className={styles.input} onClick={()=>openModalWindowDoors()}>
                                <div className={styles.input__text}>
                                    {w.quantity}
                                </div>
                                </div>
                            <div className={styles.value}>шт</div>
                        </div>
                    </div>
                    <div className={styles.properties1}>
                        <div className={styles.info__square}>
                            <div className={styles.title}>
                                <SVG className={styles.title__icon} src={wallSquare}></SVG>
                                <div className={styles.title__text}>Площадь стены</div>
                            </div>
                            <div className={styles.number}>
                                {w.width*w.height - w.doorSquare}
                                <div className={styles.number__measure}>м2</div>
                            </div>
                        </div>
                        <div className={styles.info__boxes}>
                            <div className={styles.title}>
                                <SVG className={styles.title__icon} src={wallBox}></SVG>
                                <div className={styles.title__text}>Упаковки</div>
                            </div>
                            <div className={styles.number}>
                                0
                                <div className={styles.number__measure}>шт</div>
                            </div>
                        </div>
                    </div>
                </div>
        )}
    </div>
}

export default Walls;