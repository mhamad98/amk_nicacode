import React, { useState } from "react";
import Modal from "react-modal";

import styles from './index.module.scss';
import { YMaps, Map } from 'react-yandex-maps';



export default function ModalMap(props) {

    console.log("nu pexdets")
    const [adress , setAdress] =useState([]);
    let instance;
    let MAP;
    let myPlacemark;
    let Coords = [47.21442134180362, 39.717435722344405];
    const mapData = {
        center: [47.21442134180362, 39.717435722344405],
        zoom: 10,
      };
    const onInstMap = (inst) => {
        inst.events.add('click', function (e) {
            Coords = e.get('coords');
            MAP.geocode(Coords).then(res => {
                let firstGeoObject = res.geoObjects.get(0);
                myPlacemark.properties
                .set({
                    iconCaption: [
                        firstGeoObject.getLocalities().length ? firstGeoObject.getLocalities() : firstGeoObject.getAdministrativeAreas(),
                        firstGeoObject.getThoroughfare() || firstGeoObject.getPremise(),
                        firstGeoObject.getPremiseNumber() ? firstGeoObject.getPremiseNumber() : null

                    ].filter(Boolean).join(', '),
                    balloonContent: firstGeoObject.getAddressLine()
                });
                
                //setAdress(firstGeoObject)
                //debugger
                // setAdress([
                //        firstGeoObject.getLocalities(),
                //        firstGeoObject.getAdministrativeAreas(),
                //        firstGeoObject.getThoroughfare(),
                //        firstGeoObject.getPremise(),
                //        firstGeoObject.getPremiseNumber()
                //    ])
                console.log(myPlacemark.properties._data.iconCaption)
            })


            if (myPlacemark) {
                myPlacemark.geometry.setCoordinates(Coords);
            }
            else {
                myPlacemark = createPlacemark(Coords);
                inst.geoObjects.add(myPlacemark);
            }
        })
    }
    function createPlacemark() {
        return new MAP.Placemark(Coords, {
            iconCaption: 'поиск...'
        }, {
            preset: 'islands#violetDotIconWithCaption',
        });
    }

    return <Modal isOpen={props.show}
                className={styles.modal}
                overlayClassName={styles.modalWrapper}
                onRequestClose={props.hideModal}
                >
                    <div className={styles.header}>
                        <div className={styles.title}>Карта</div>
                        <div className={styles.ready}>Готово</div>
                    </div>
                    <div className={styles.mymap}>
                        <YMaps 
                        className={styles.ymap}
                        query={{
                            apikey:"e3619e62-83c8-479b-9acc-1016397c1a42",
                            load: ["package.full"],
                          }}
                        >
                            <Map className={styles.ymap} 
                            defaultState={mapData} 
                            onLoad={(inst) => MAP = inst}
                            modules={["geolocation", "geocode"]}
                            instanceRef={inst => {instance = inst}}
                            onClick={(e) => onInstMap(instance,e)}
                            >
                            </Map>
                        </YMaps>
                    </div>
                    <div className={styles.footer}>
                        <div className={styles.address}>{adress} </div>
                    </div>
    </Modal>
}  
