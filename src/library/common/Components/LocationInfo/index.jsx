import React, { useState } from "react";
import SVG from 'react-inlinesvg';
import locationFlag from '../../../../resources/images/locationFlag.svg'

import styles from "./index.module.scss";

import ModalMap from './LocationInfoFrames/mapModal'

function LocationInfo(props) {

    const [ modalMap, setModalMap ] = useState(false);

    return <div className={styles.LocationInfo}>
        <ModalMap show={modalMap} />
        <div className={styles.header}>
            <div className={styles.LocationInfo__title}>Адрес объекта</div>
            <div className={styles.location} onClick={()=>setModalMap(true)}>
                <SVG src={locationFlag}/>
                <div className={styles.text}>Указать на карте</div>
            </div>
        </div>
        <div className={styles.LocationInfo__data}>
            <div className={styles.container}>
                <div className={styles.title}>Город / Населенный пункт</div>
                <input className={styles.input} placeholder="Город / Населенный пункт"/>
            </div>
            <div className={styles.container}>
                <div className={styles.title}>Улица</div>
                <input className={styles.input} placeholder="Улица"/>
            </div>
            <div className={styles.container}>
                <div className={styles.title}>Дом</div>
                <input className={styles.input} placeholder="Номер дома"/>
            </div>
            <div className={styles.location} onClick={()=>setModalMap(true)}>
                <SVG src={locationFlag}/>
                <div className={styles.text}>Указать на карте</div>
            </div>
        </div>
    </div>
}

export default LocationInfo;