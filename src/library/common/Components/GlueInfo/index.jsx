import React, { useState } from "react";
import SVG from 'react-inlinesvg';

import styles from "./index.module.scss";

import glue from 'resources/images/glue.svg'

function GlueInfo(props) {
    return <div className={styles.GlueInfo}>
        <div className={styles.GlueInfo__needed}>
            <SVG src={glue}/>
            <div className={styles.text}>Вам понадобиться 3 упаковки клея</div>
            <div className={styles.btn}>?</div>
        </div>
        <div className={styles.GlueInfo__slash}/>
        <div className={styles.GlueInfo__date}>Срок изготовления: 10 дней со дня оплаты</div>
    </div>
}

export default GlueInfo
