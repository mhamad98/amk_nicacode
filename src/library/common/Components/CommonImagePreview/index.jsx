import React from 'react';
import styles from './index.module.scss';
import SVG from 'react-inlinesvg';

import uploadImg from 'resources/images/uploadImg.svg'

export default class ImagePreveiw extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      url: ''
    }
    this.generatePreviewImgUrl = this.generatePreviewImgUrl.bind(this);
    this.generatePreviewImgUrl(this.props.image);
  }


  componentWillReceiveProps(nextProps){
    if(this.props.image !== nextProps.image){
      this.generatePreviewImgUrl(nextProps.image);
    }
  }

  generatePreviewImgUrl(file) {
    if(file){
      let reader = new FileReader()
      let url = reader.readAsDataURL(file)
      console.log("./src/library/common/Components/CommonImagePreview/index.jsx:28", url)
      reader.onloadend = (e => {
        this.setState({
          url: reader.result
        })
      })
    }
    
  }

  render() {
    return(
        <div className={styles.images__item} style={this.props.style}>
          <img src={this.state.url} alt=""/>
          <SVG className={styles.upload} src={uploadImg}/>
        </div>
    )
  }
}
