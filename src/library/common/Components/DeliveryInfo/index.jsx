import React, { useState } from "react";
import SVG from 'react-inlinesvg';

import packages from 'resources/images/packages.svg'
import packages1 from 'resources/images/packages1.svg'
import weightkg from 'resources/images/weightkg.svg'
import weightkg1 from 'resources/images/weightkg1.svg'

import styles from "./index.module.scss";

function DeliveryInfo(props) {
    return <div className={styles.DeliveryInfo}>
        <div className={styles.header}>
            <div className={styles.DeliveryInfo__title}>Доставка</div>
            <div className={styles.measures}>
                <div className={styles.space1}>
                    <SVG src={packages1}/>
                    <div className={styles.subtext1}>264 кг</div>
                </div> 
                <div className={styles.weight1}>
                    <SVG src={weightkg1}/>
                    <div className={styles.subtext1}>42 м2</div>
                </div>
            </div>
        </div>
        <div className={styles.DeliveryInfo__info}>
            <div className={styles.text}>ФИО получателя</div>
            <input className={styles.fio} placeholder='ФИО получателя'/>
            <div className={styles.text}>Контактный номер</div>
            <input className={styles.number} placeholder='Контактный номер'/>
            <div className={styles.text}>Город</div>
            <input className={styles.city} placeholder='Город'/>
            <div className={styles.space}>
                <SVG src={packages}/>
                <div className={styles.subtext}>264 кг</div>
            </div>  
            <div className={styles.weight}>
                <SVG src={weightkg}/>
                <div className={styles.subtext}>42 м2</div>
            </div>
        </div>
    </div>
}

export default DeliveryInfo
