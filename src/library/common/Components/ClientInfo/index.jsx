import React, { useState } from "react";

import styles from "./index.module.scss";
import { Select } from 'semantic-ui-react'

function ClientInfo(props) {

    const styleLink = document.createElement("link");
    styleLink.rel = "stylesheet";
    styleLink.href = "https://cdn.jsdelivr.net/npm/semantic-ui/dist/semantic.min.css";
    document.head.appendChild(styleLink);

    const [ ip, setIp] = useState('none');
    const rightOption = [
        {
          key: 'Физическое лицо',
          text: 'Физическое лицо',
          value: 'Физическое лицо',
          disabled: false
          
        },
        {
          key: 'ИП',
          text: 'ИП',
          value: 'ИП',
          disabled: false
          
        },
        {
          key: 'ЗАО',
          text: 'ЗАО',
          value: 'ЗАО',
        },
        {
          key: 'ЧЛ',
          text: 'ЧЛ',
          value: 'ЧЛ',
          
        },
        {
          key: 'ППО ООО',
          text: 'ППО ООО',
          value: 'ППО ООО',
          
        },
        {
          key: 'Другое',
          text: 'Другое',
          value: 'Другое',
        
        },
      ]

      function inputHaddler(e) {
        if (e.target.innerHTML == '<span class="text">ИП</span>') {
          setIp('flex')
        }
        else {
          setIp('none')
        }
      }

    return <div className={styles.ClientInfo}>
        <div className={styles.ClientInfo__title}>Клиент</div>
        <div className={styles.ClientInfo__data}>
          <div className={styles.container}>
            <div className={styles.title}>Правовая форма</div>
            <Select className={styles.select} options={rightOption} placeholder="Правовая форма" onChange={e => inputHaddler(e)}/>
          </div>
          <div className={styles.container}>
            <div className={styles.title}>ФИО</div>
            <input className={styles.input} placeholder="ФИО"/>
          </div>
          <div className={styles.container}>
            <div className={styles.title}>Контактный номер</div>
            <input className={styles.input} placeholder="Контактный номер"/>
          </div>
          <div className={styles.container}>
            <div className={styles.title}>E-mail</div>
            <input className={styles.input} placeholder="E-mail"/>
          </div>
        </div>
        <div className={styles.ClientInfo__ip} style={{display:ip}}>
          <div className={styles.inn}>
            <div className={styles.title}>ИНН</div>
            <input className={styles.input}/>
          </div>
          <div className={styles.account}>
            <div className={styles.title}>Расчетный счет</div>
            <input className={styles.input}/>
          </div>
          <div className={styles.address}>
            <div className={styles.title}>Адрес</div>
            <input className={styles.input}/>
          </div>
          <div className={styles.allName}>
            <div className={styles.title}>Полное название ИП</div>
            <input className={styles.input}/>
          </div>
          <div className={styles.factAdress}>
            <div className={styles.title}>Фактический адрес</div>
            <input className={styles.input}/>
          </div>
          <div className={styles.mailIndex}>
            <div className={styles.title}>Почтовый адрес</div>
            <input className={styles.input}/>
          </div>
          <div className={styles.ogrn}>
            <div className={styles.title}>ОГРН</div>
            <input className={styles.input}/>
          </div>
          <div className={styles.okpo}>
            <div className={styles.title}>ОКПО</div>
            <input className={styles.input}/>
          </div>
          <div className={styles.corrporat}>
            <div className={styles.title}>Корр/счет</div>
            <input className={styles.input}/>
          </div>
          <div className={styles.bik}>
            <div className={styles.title}>БИК</div>
            <input className={styles.input}/>
          </div>
        </div>
    </div>
}

export default ClientInfo