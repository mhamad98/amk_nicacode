import React, {useState} from "react";

import { NavLink, withRouter } from 'react-router-dom';
import {connect} from 'react-redux';
import SVG from 'react-inlinesvg';
import { removeAuthentication } from 'library/common/Actions/authActions';

import styles from "./index.module.scss";

import line from "../../../../resources/images/horLine.svg";
import profile from "../../../../resources/images/profile_menu.svg";
import catalog from "../../../../resources/images/catalog.svg";
import example from "../../../../resources/images/example.svg";
import Order from "../../../../resources/images/Order.svg";
import client from "../../../../resources/images/client1.svg";
import support from "../../../../resources/images/support.svg";
import horLine1 from "../../../../resources/images/horLine1.svg";
import bell from "../../../../resources/images/bell.svg";
import logo from "../../../../resources/images/logo.svg";
import logo1 from "../../../../resources/images/logo_1.svg";
import gift from "../../../../resources/images/gift.svg";
import statistic from "../../../../resources/images/statistic.svg";
import legalAgreement from "../../../../resources/images/legalAgreement.svg";
import myData from "../../../../resources/images/myData.svg";
import exit from "../../../../resources/images/exit.svg";
import profileDropArrow from "../../../../resources/images/profileDropArrow.svg";

import ProfileAutoModal from "modules/Profile/ProfileFrames/ProfileAutoModal"

function Menu(props) {
    function logOut() {
        props.removeAuthentication()
        props.history.push('/')
      }

    const [dropmenu, setDropmenu] = useState('none')
    const [arrow, setArrow] = useState('none')
    const [authModal, setAuthoModal] = useState(false)
    const [autharized, setAutharized] = useState(false)

    function openModal() {
        if (autharized) {
            
        }

        else {
            setAuthoModal(true)
        }
    }

    function closeModal() {
        setAuthoModal(false)
    }

    function showmenu() {
        if (dropmenu === 'none') {
            setDropmenu('flex')
        }
        else {
            setDropmenu('none')
        }
    }
    function passwordConfirmed(val) {
        setArrow('block')
        setDropmenu('flex')
    }

    return (
        <div className={styles.grid}> 
            <div className={styles.grid__logo}>
                <SVG alt='logo' src = {logo} />
            </div>
            <div className={styles.grid__logo1}>
                <SVG alt='logo' src = {logo1} />
            </div>
            <SVG className={styles.grid__horLine1} src={horLine1} alt="horLine"/>
            <NavLink className={styles.grid__orders} to="/orders" activeClassName={styles.info_active}>
                <div className={styles.orders__info} >
                    <div className={styles.info__quantityIn}>4</div>
                    <SVG alt='order icon' className={styles.info__icon} src={Order}/>
                    <div className={styles.info__text}>Заказы</div>
                </div>
                <div className={styles.info__quantity}>4</div>
            </NavLink>
            <NavLink className={styles.grid__clients} to="/clients" activeClassName={styles.info_active}>
                <div className={styles.orders__info} >
                    <div className={styles.info__quantityIn}>4</div>
                    <SVG alt='clients icon' className={styles.info__icon} src={client}/>
                    <div className={styles.info__text}>Клиенты</div>
                </div>
                <div className={styles.info__quantity}>4</div>
            </NavLink>
            <NavLink className={styles.grid__catalog} to="/catalog" activeClassName={styles.info_active}>
                <div className={styles.orders__info}>
                    <SVG alt='catalog icon' className={styles.info__icon} src={catalog}/>
                    <div className={styles.info__text}>Каталог</div>
                </div>
                <div className={styles.info__quantity}>4</div>
            </NavLink>
            <NavLink className={styles.grid__examples} to="/examples"activeClassName={styles.info_active}>
                <div className={styles.orders__info}>
                    <SVG alt='example icon' className={styles.info__icon} src={example}/>
                    <div className={styles.info__text}>Примеры работ</div>
                </div>
                <div className={styles.info__quantity}>4</div>
            </NavLink>
            <SVG alt='' className={styles.grid__horLine} src={line}/>
            <div className={styles.grid__profile}>
                <div className={styles.container}>
                    <div className={styles.orders__info} onClick={()=>openModal()}>
                        <SVG alt='profile icon' className={styles.info__icon} src={profile}/>
                        <div className={styles.info__text}>Профиль</div>
                    </div>
                    <ProfileAutoModal
                    confirm={passwordConfirmed}
                    show={authModal}
                    hideModal={closeModal}
                    authorized={setAutharized}
                    />
                    <div className={styles.arrowContain} onClick={()=>showmenu()}>
                        <SVG className={styles.arrow + " " + (dropmenu === 'flex' ? styles.arrow_active : "")}  src={profileDropArrow} style={{display:arrow}}/>
                    </div>
                </div>
                <div className={styles.dropmenu} style={{display:dropmenu}}>
                    <NavLink className={styles.orders__info} to="/profile" activeClassName={styles.info_active}>
                        <SVG alt='profile icon' className={styles.info__icon} src={myData}/>
                        <div className={styles.info__text}>Мои данные</div>
                    </NavLink>
                    <NavLink className={styles.orders__info} to="/statistic" activeClassName={styles.info_active}>
                        <SVG alt='profile icon' className={styles.info__icon} src={statistic}/>
                        <div className={styles.info__text}>Статистика продаж</div>
                    </NavLink>
                    <NavLink className={styles.orders__info} id={styles.gift} to="/gift" activeClassName={styles.info_active}>
                        <SVG alt='profile icon' className={styles.info__icon} src={gift}/>
                        <div className={styles.info__text}>Витрина подарков</div>
                    </NavLink>
                    <NavLink className={styles.orders__info} to="/legalAgreement" activeClassName={styles.info_active}>
                        <SVG alt='profile icon' className={styles.info__icon} src={legalAgreement}/>
                        <div className={styles.info__text}>Юр. соглашение</div>
                    </NavLink>
                    <div className={styles.orders__info} onClick={() => logOut()}>
                        <SVG alt='profile icon' className={styles.info__icon} src={exit}/>
                        <div className={styles.info__text}>Выйти</div>
                    </div>
                </div>
            </div>
            <div className={styles.grid__bell}>
                <NavLink className={styles.orders__info} to="/bell" activeClassName={styles.info_active}>
                    <SVG alt='bell icon' className={styles.info__icon} src={bell}/>
                    <div className={styles.info__text}>Уведомления</div>
                </NavLink>
            </div>
            <div className={styles.grid__support}>
                <NavLink className={styles.orders__info} to="/support" activeClassName={styles.info_active}>
                    <SVG alt='support icon' className={styles.info__icon} src={support}/>
                    <div className={styles.info__text}>Тех поддержка</div>
                </NavLink>

            </div>
        </div>
    )
}
const mapStateToProps = state => ({
    logOut: state.authReducer
  });
  
  const mapDispatchToProps = {
    removeAuthentication
  };
  

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Menu));