import React from "react";

import styles from "./index.module.scss";

import SMS from "../../../../resources/images/SMS.svg";
import bell from "../../../../resources/images/bell.svg";
import profile from "../../../../resources/images/profile_header.svg";
import horLine from "../../../../resources/images/verLine.svg";
import navBar from "../../../../resources/images/navBar.svg";
import search from "../../../../resources/images/search.svg";

function Header(props) {
    return (
        <div className={styles.header}>
            <div className={styles.headerpageInfo}>
                <img alt='bar icon' className={styles.headerpageInfo__navBar} src={navBar} />
                <div className={styles.headerpageInfo__title}>Заказы</div>
                <img alt='search icon' className={styles.headerpageInfo__search} src={search} />
                <img alt='SMS icon' className={styles.headerpageInfo__SMS} src={SMS}/>
                <img alt='horizontal line' className={styles.headerpageInfo__line} src={horLine}/>
                <div className={styles.headerpageInfoprofile}>
                    <div className={styles.headerpageInfoprofile__name}>Иванова Дарья</div>
                    <img alt='profile icon' className={styles.headerpageInfoprofile__icon} src={profile}/>
                </div>
            </div>
        </div>
    )
}

export default Header;