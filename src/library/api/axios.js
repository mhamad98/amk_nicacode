import Axios from 'axios';
import { getTokenToken } from 'library/utilities/token';
import errorHandler from './errorHandler';

const axiosInstance = Axios.create({
  headers: { "Content-Type": "application/json" },
});

axiosInstance.interceptors.request.use(config => {
  const token = getTokenToken();
  const clonedConfig = config;
  if (typeof(token) != 'undefined') {
    clonedConfig.headers.common = {
      Authorization: `Token ${token}`,
      'Content-Type': 'application/json',
    };
  }
  return clonedConfig;
}
);

axiosInstance.interceptors.response.use(
  config => config,
  error => {
    errorHandler(error);
    return Promise.reject(error);
  },
);

export default axiosInstance;
