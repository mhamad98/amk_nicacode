import axios from 'library/api/axios';
import config from 'main/config';

const URL = `${config.API_BASE_URI}/api/v1/`;

export const Auth = (data) => axios.post(`${URL}auth/login/`,data);