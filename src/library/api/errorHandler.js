import {showErrorServer} from 'library/common/Actions/errorServerActions';
import store from 'main/store/configureStore';
import { setCookie  } from 'library/utilities/cookie';

const errorHandler = error => {
  if (error.response) {

    if(error.response && error.response.status === 401){
      setCookie('csrftoken', '', {expires: -1});
      setCookie('token', '', {expires: -1});
      if(window.location.pathname !== '/'){
        window.location = '/';
      }
    }
    if(error.response.status >= 500) {
      store.dispatch(showErrorServer(`Maintenance in progress. Come back shortly - (${error.response.status})`))
      if(window.location.pathname !== '/'){
        window.location = '/';
      }

    }

    if(error.response.status === 404){
      store.dispatch(showErrorServer(`Page or data was not found - (${error.response.status})`))
    }
  }
};

export default errorHandler;