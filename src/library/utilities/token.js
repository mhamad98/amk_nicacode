//import Storage from 'library/utilities/storage';
import { setCookie, getCookie } from 'library/utilities/cookie';


export const setToken = data => { setCookie('csrftoken', data); };

export const getToken = () => getCookie('csrftoken')

export const setTokenToken = data => { setCookie('token', data); };

export const getTokenToken = () => getCookie('token')

export const removeToken = () => setCookie('csrftoken', '', {expires: -1})

export const removeTokenToken = () => setCookie('token', '', {expires: -1})