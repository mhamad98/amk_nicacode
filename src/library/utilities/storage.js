export default class Storage {
    static getItem(name) {
      return JSON.parse(localStorage.getItem(name));
    }
  
    static setItem(name, value) {
      localStorage.setItem(name, JSON.stringify(value));
    }
  
    static removeItem(name) {
      return localStorage.removeItem(name);
    }
  }
  