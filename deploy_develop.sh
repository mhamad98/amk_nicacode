#!/bin/bash

LOG_DIR=./logs/nginx/
DOCKER_COMPOSE_FILE=docker-compose.yml

if [ ! -d $LOG_DIR ]; then
   mkdir -p $LOG_DIR
fi

git fetch --all
git reset --hard origin/developer

docker-compose -f $DOCKER_COMPOSE_FILE down
docker-compose -f $DOCKER_COMPOSE_FILE up -d
